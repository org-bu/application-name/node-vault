const express = require('express');
const router = express.Router();

var options = {
  apiVersion: 'v1', // default
  endpoint: process.env.VAULT_URL, // default
  token: process.env.VAULT_TOKEN // optional client token; can be fetched after valid initialization of the server
};

// get new instance of the client
var vault = require("node-vault")(options);

/* GET secret listing. */
router.get('/', async function (req, res, next) {
  // Read the secret from Vault
  const secret = await vault.read('secret/data/foo').catch(err => {
    res.send(err);
  }); //secret path
  console.log(secret.data);

  res.send(`secret is ${secret.data}`);
});

module.exports = router;
